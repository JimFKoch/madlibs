//Group Members:
//Zachary Brandt
//James Kockh
//Logan Weyers
//Seth Glisczinski

//Mad Libs Project

#include <iostream>
#include <conio.h>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

int main()
{
	string answer[12] = { "adjective", "sport", "city", "person", "action verb(past tense)", "vehicle", "place", "noun (plural)", "adjective", "food (plural)", "liquid", "adjective"};

	for (int i = 0; i < 12; i++)
	{
		cout << "Enter a " << answer[i] << ": ";
		getline(cin, answer[i]);
	}
	
	string madlib = "One day my " + answer[0] 
		 + " friend and I decided to go to the " + answer[1] 
		 + " game in " + answer[2]
		 + ".\n We really wanted to see " + answer[3]
		 + " play.\n So we " + answer[4] 
		 + " in the " + answer[5] 
		 + " and headed down to " + answer[6] 
		 + " and bought some " + answer[7] 
		 + ".\n We watched the games and it was " + answer[8] 
		 + ".\n We ate some " + answer[9] 
		 + " and drank some " + answer[10] 
		 + ".\n We had a " + answer[11] + " time, and can't wait to go again.\n";

	cout << madlib;
	cout << "\n\n";

	cout << "Would you like to save your Mad Libs? (y/n)";

	string input;
	cin >> input;
	
	if (input == "y")
	{
		string path = "C:\\Users\\public\\madlib.txt";
		ofstream ofs(path);

		ofs << madlib;
		ofs.close();

		cout << "Your mad libs has been saved!";
	}
	else
		cout << "Your mad libs has not been saved.";

	(void)_getch;
	return 0;
}